const db     = require('../config/index');
const jwt    = require('jsonwebtoken');
var mongoose = require('mongoose'); 
var express  = require('express');
var router   = express.Router();
let tblModel = require('../models/coll_users')
var bcrypt = require('bcrypt');
const config={
    secret:'kdsfgsnkldgjxnsl345685439'
};
router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
  });
/* GET users listing. */
router.post('/login',async function(req,res,next)
{
    tblModel.findOne({email:req.body.email}, async function (err, datatemp)
    {
        if(datatemp!=null)
        {
            var validate = await new Promise(async (resolve, reject) => 
            {
                console.log({form:req.body.password, passuser:datatemp});
                bcrypt.compare(req.body.password, datatemp.password, function(err, resultado) {
                    console.log({err, resultado});
                    if (typeof err === 'undefined') 
                    {
                        resolve(resultado);
                    }
                    else
                    {
                        resolve(false);
                        reject(false);
                    }
                });
            });
            if(validate)
            {
                let token = jwt.sign({ id: datatemp._id }, config.secret, { expiresIn: 86400 // expires in 24 hours
                });
                delete datatemp.password;
                res.status(200).send({ auth: true, token: token, user: datatemp,validate:true,msj:null });
            }
            else{
                res.send({auth: true, token: null, user:null,validate:false,msj:'La contraseña no coincide'});
            }
        }
        else
        {
            res.send({validate:false,msj:'El usuario no existe'});
        }
    });
});

router.post('/save', async function(req, res, next)
{
    let rq = req.body;
    console.log({rq:rq,documento:rq.documento});
    let password = bcrypt.hashSync(rq.documento, 10);
    let datasave =
    {
        _id: new mongoose.Types.ObjectId(),
        nombre_primero:     rq.nombre_primero,
        nombre_segundo:     rq.nombre_segundo,
        apellido_segundo:   rq.apellido_segundo,
        apellido_primero:   rq.apellido_primero,
        email:              rq.email,
        documento_numero:   rq.documento,
        password:           password
    };
    tblModel.create(datasave,function(error,response)
    {
        res.send({validate:true,data:response});
    })
});

router.post('/changePass', async function(req, res, next)
{
    let rq = req.body;
    let respuesta = false;
    let usr=JSON.parse(rq.local.user);
    pass=usr.password;
    if(usr.email==rq.user)
    {
        if(rq.passNew1==rq.passNew2)
        {
            respuesta = await new Promise((resolve, reject) => 
            {
                bcrypt.compare(rq.passold, pass, function(err, resp) {
                    if (err) reject(err)
                    resolve(resp)
                });
            });
            if(respuesta)
            {
                var password = await new Promise((resolve, reject) => 
                {
                    bcrypt.hash(rq.passNew1, 10, function(err, hash) 
                    {
                        if (err) reject(err)
                        resolve(hash)
                    });
                });
                tblModel.updateOne({email:usr.user},{password:password})
                .then(function(error,response)
                {
                    res.send({validate:true,error:error,data:response});
                })
            }
        }
        else{
            respuesta=false;
        }
    }
    else
    {
        resultado=false;
    }
    res.send({validate:respuesta});
});

module.exports = router;