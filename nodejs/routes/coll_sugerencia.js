var mongoose = require('mongoose'); 
var express  = require('express');
var router   = express.Router();
let tblModel = require('../models/coll_sugerencia')
router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
  });
/* GET users listing. */
router.post('/', function(req, res, next) 
{
    let rq = req.body;
    tblModel.paginate({}, {sort:{'_id':-1}, page: rq.pag, limit: 10 }, function (err, datatemp)
    {
        if (err) return handleError(err);
        res.send({validate:true,data:datatemp});
    });
});
router.post('/find',function(req,res,next)
{
    tblModel.findById(req.body._id, function (err, datatemp)
    {
        res.send({validate:true,data:datatemp});
    });
})
router.post('/save', function(req, res, next)
{
    let rq = req.body;
    console.log({rq:rq });
    let datasave =
    {
        _id: new mongoose.Types.ObjectId(),
        description :rq.description,
        fecha: new Date()
    };
    tblModel.create(datasave,function(error,response)
    {
        res.send({validate:true,data:response});
    })
});
module.exports = router;