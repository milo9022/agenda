
var mongoose = require('mongoose'); 
var express  = require('express');
var router   = express.Router();
let tblModel = require('../models/coll_specialties')
let updates          = require('../controls/updates');
router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
  });


/* GET users listing. */
router.get('/', function(req, res, next) 
{
    tblModel.find({}, function (err, datatemp)
    {
        if (err) return handleError(err);
        res.send({validate:true,data:datatemp});
    });
});
router.post('/find',function(req,res,next)
{
    tblModel.findById(req.body.id, function (err, datatemp)
    {
        res.send({validate:true,data:datatemp});
    });
})
router.post('/save', function(req, res, next)
{
    let rq = req.body;
    let datasave =
    {
        _id: new mongoose.Types.ObjectId(),
        name        :rq.name,
        description :rq.description
    };
    tblModel.create(datasave,function(error,response)
    {
        res.send({validate:true,data:response});
    })
});
router.post('/update', async function(req, res, next)
{
    let rq = req.body;
    let datasave =
    {
        '_id'           :new mongoose.Types.ObjectId(rq._id),
        'name'          :rq.name,
        'description'   :rq.description
    }
    let resultado=await updates.updateEspecialidades(rq._id,datasave);
    res.send({validate:true,res:resultado});
});
module.exports = router;