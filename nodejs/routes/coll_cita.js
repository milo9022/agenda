var mongoose     = require('mongoose');
mongoose.set('debug',true);
const db            = require('../config/index');
var express         = require('express');
var router          = express.Router();
let tblPatient      = require('../models/coll_patients');
let tblEspecilaista = require('../models/coll_especialista');
let documentType    = require('../models/coll_documents_types');
let citas           = require('../models/coll_citas');
let tblEps          = require('../models/coll_eps')
let citasEstado     = require('../models/coll_citas_estado');

function formatData(rq)
{
    rq.paciente._id                        = new mongoose.Types.ObjectId(rq.paciente._id);
    rq.paciente.documentoType._id          = new mongoose.Types.ObjectId(rq.paciente.documentoType._id);
    rq.especialista._id                    = new mongoose.Types.ObjectId(rq.especialista._id);
    rq.especialista.documento_tipo._id     = new mongoose.Types.ObjectId(rq.especialista.documento_tipo._id);
    rq.especialista.especialidad_tipo._id  = new mongoose.Types.ObjectId(rq.especialista.especialidad_tipo._id);
    rq.especialista._id                    = new mongoose.Types.ObjectId(rq.especialista._id);
    rq.eps._id                             = new mongoose.Types.ObjectId(rq.eps._id);
    rq.fecha = new Date(rq.fecha);
    return rq;
}
function formatDate(fecha)
{
    var fechas = new Date(fecha);
}
router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
});

function searchDocument(documento)
{
    let res = observables.finder.findOne(tblPatient, {documento:'1061716139'});
    return 123;
}
/* GET users listing. */
router.get('/test', function(req, res, next) 
{
    searchDocument(null)
    res.send({text:123});
});
router.get('/prueba', function(req, res, next) 
{
    
});
router.get('/', function(req, res, next) 
{
    tblPatient.find({}, function (err, datatemp)
    {
        if (err) return handleError(err);
        res.send({validate:true,data:datatemp});
    });
});
router.post('/find',function(req,res,next)
{
    citas.findById(req.body.id, function (err, datatemp)
    {
        res.send({validate:true,data:datatemp});
    });
});
router.post('/save', async function(req, res, next)
{
    let rq = req.body;
    var documentTypes = await new Promise((resolve, reject) => 
    {
        documentType.findById(rq.paciente.documentoType, function(err, resultado) 
      {
        if(typeof resultado!='undefined')
        {
          if (err){ reject(err)}else{resolve(resultado)}
        }
        else
        {
          resolve({data:null});
        }
      });
    });
    var citaEstado = await new Promise((resolve, reject) =>
    {
        citasEstado.findById('5d9c013ae96ebbae8422e2a9', function(err, resultado) 
        {
          if(typeof resultado!='undefined'){if (err){ reject(err)}else{resolve(resultado)}}
          else{resolve({data:null});}
        });
      })
      var especialista = await new Promise((resolve, reject) => 
      {
        tblEspecilaista.findById(rq.especialista, function(err, resultado) 
        {
        if(typeof resultado!='undefined')
        {
          if (err){ reject(err)}else{resolve(resultado)}
        }
        else
        {
          resolve({data:null});
        }
      });
    });
    var eps = await new Promise((resolve, reject) => 
    {
        tblEps.findById(rq.eps, function(err, resultado) 
        {
          if(typeof resultado!='undefined')
          {
            if (err){ reject(err)}else{resolve(resultado)}
          }
          else
          {
            resolve({data:null});
          }
        });
    });
    rq.paciente.documentoType=documentTypes;
    var paciente = await new Promise((resolve, reject) => 
    {
        tblPatient.findOne({documento:rq.paciente.documento}, async function(err, resultado) 
        {
            if(resultado==null)
            {
                rq.paciente._id = new mongoose.Types.ObjectId();
                await tblPatient
                .create(rq.paciente,function(failud,success)
                {
                    if(failud==null)
                    {
                        resolve(rq.paciente)
                    }
                })
            }
            else{
                if(typeof resultado!='undefined')
                {

                    if (err){ reject(err)}else{resolve(resultado)}
                }
                else
                {
                    resolve({data:null});
                }
            }
        });
    });
    rq.paciente=paciente;
    rq.estado=citaEstado;
    rq.especialista=especialista;
    rq.eps=eps;
    rq._id=new mongoose.Types.ObjectId();
    citas.create(rq,function(err,result)
    {
      res.send(result);
    });
});
router.post('/update', async function(req, res, next)
{
    
    let rq = req.body;
    var documentTypes = await new Promise((resolve, reject) => 
    {
        documentType.findById(rq.paciente.documentoType, function(err, resultado) 
      {
        if(typeof resultado!='undefined')
        {
          if (err){ reject(err)}else{resolve(resultado)}
        }
        else
        {
          resolve({data:null});
        }
      });
    });
    var citaEstado = await new Promise((resolve, reject) =>
    {
        citasEstado.findById('5d9c013ae96ebbae8422e2a9', function(err, resultado) 
        {
            if(typeof resultado!='undefined'){if (err){ reject(err)}else{resolve(resultado)}}
            else{resolve({data:null});}
        });
    })
    var especialista = await new Promise((resolve, reject) => 
    {
        tblEspecilaista.findById(rq.especialista, function(err, resultado) 
      {
        if(typeof resultado!='undefined')
        {
          if (err){ reject(err)}else{resolve(resultado)}
        }
        else
        {
          resolve({data:null});
        }
      });
    });
    var eps = await new Promise((resolve, reject) => 
    {
        tblEps.findById(rq.eps, function(err, resultado) 
        {
          if(typeof resultado!='undefined')
          {
            if (err){ reject(err)}else{resolve(resultado)}
          }
          else
          {
            resolve({data:null});
          }
        });
    });
    rq.paciente.documentoType=documentTypes;
    var paciente = await new Promise((resolve, reject) => 
    {
        tblPatient.updateOne({_id: new mongoose.Types.ObjectId(rq.paciente._id)},rq.paciente, function(err, resultado) 
        {
            if (err){ reject(err)}else{resolve(resultado)}
        });
    });
    rq.paciente=paciente;
    rq.estado=citaEstado;
    rq.especialista=especialista;
    rq.eps=eps;
    citas.updateOne({_id: new mongoose.Types.ObjectId(rq._id)},rq,function(err1,resut)
    {
        res.send({err1,resut}); 
    })
});

router.post('/consultar',function(req,res,next)
{
    let rq   = req.body;
    citas.find({"paciente.documento":rq.documento},function(error,result)
    {
        res.send({data:result});
    });
});

router.post('/all',function(req,res,next)
{
    let rq   = req.body;
    citas.paginate({}, {sort:{'_id':-1}, page: rq.pag, limit: 10 }, function (err, datatemp)
    {
        if (err) return handleError(err);
        res.send({validate:true,data:datatemp});
    });
});

router.post('/hours',function(req,res,next)
{
    let rq   = req.body;
    let res1 = [];
    let hora = 
    [

        {hora:'07:00 P.M.',disponible:true},
        {hora:'07:15 P.M.',disponible:true},
        {hora:'07:30 P.M.',disponible:true},
        {hora:'07:45 P.M.',disponible:true},
        {hora:'08:00 A.M.',disponible:true},
        {hora:'08:15 A.M.',disponible:true},
        {hora:'08:30 A.M.',disponible:true},
        {hora:'08:45 A.M.',disponible:true},

        {hora:'10:45 A.M.',disponible:true},
        {hora:'11:00 A.M.',disponible:true},
        {hora:'11:15 A.M.',disponible:true},
        {hora:'11:30 A.M.',disponible:true},
        {hora:'11:45 A.M.',disponible:true},
        {hora:'12:00 P.M.',disponible:true},
        {hora:'12:15 P.M.',disponible:true},
        {hora:'12:30 P.M.',disponible:true},
        {hora:'12:45 P.M.',disponible:true},
        {hora:'01:00 P.M.',disponible:true},
        {hora:'01:15 P.M.',disponible:true},
        {hora:'01:30 P.M.',disponible:true}
    ];
    rq.fecha=rq.fecha.split('T');
    let now     = new Date(rq.fecha[0]);
    let start   = new Date(now.getFullYear(),now.getMonth(),now.getDate()+0,0,0,0);
    let end     = new Date(now.getFullYear(),now.getMonth(),now.getDate()+1,0,0,0);
    citas.find({'fecha':{$gte:start,$lt: end}},function(error,result)
    {
        for(var i = 0; i<result.length;i++)
        {
            let rs      = result[i];
            let gethora = hora.find(hour => hour.hora === rs.hora);
            if(typeof gethora !== 'undefined')
            {
                gethora.disponible=false;
                hora[hora.indexOf(gethora)]=gethora;
            }
        }
        res.send({hora:hora,result:result});
    });    
})
router.post('/delete', function(req, res, next)
{
    let rq = req.body;
    citas.remove({_id:new mongoose.Types.ObjectId(rq._id)},function(error)
    {
        res.send({error:error});
    }).catch(function(datares)
    {
        res.send({catch:datares});
    });
});
module.exports = router;