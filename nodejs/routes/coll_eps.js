
var mongoose = require('mongoose'); 
var express  = require('express');
var router   = express.Router();
let tblModel = require('../models/coll_eps')
let updates  = require('../controls/updates');

router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
  });

//FUNCTIONS
function updateothers(data,_id)
{
    console.log(data,_id);
}

/* GET users listing. */
router.get('/', function(req, res, next) 
{
    tblModel.find({}, function (err, datatemp)
    {
        if (err) return handleError(err);
        res.send({validate:true,data:datatemp});
    });
});
router.post('/find',function(req,res,next)
{
    tblModel.findById(req.body.id, function (err, datatemp)
    {
        res.send({validate:true,data:datatemp});
    });
})
router.post('/save', function(req, res, next)
{
    let rq = req.body;
    let datasave =
    {
        _id: new mongoose.Types.ObjectId(),
        name        :rq.name,
        description :rq.description
    };
    tblModel.create(datasave,function(error,response)
    {
        res.send({validate:true,data:response});
    })
});
router.post('/update', async function(req, res, next)
{
    let rq = req.body;
    let datasave =
    {
        'name'          : rq.name,
        'description'   : rq.description,
        _id             : new mongoose.Types.ObjectId(rq._id)
    }
    let eps = await updates.updateEps(rq._id,datasave);
    res.send({eps:eps});
});
router.post('/delete', function(req, res, next)
{
    let rq = req.body;
    tblModel.remove({_id:new mongoose.Types.ObjectId(rq._id)},function(error)
    {
        res.send({error:error});
    }).catch(function(datares)
    {
        res.send({catch:datares});
    });
});
module.exports = router;