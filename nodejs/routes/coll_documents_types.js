const db     = require('../config/index');
var mongoose = require('mongoose'); 
var express  = require('express');
var router   = express.Router();
let tblModel = require('../models/coll_documents_types')
router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
  });
/* GET users listing. */
router.get('/', function(req, res, next) 
{
    tblModel.find({}, function (err, datatemp)
    {
        if (err) return handleError(err);
        res.send({validate:true,data:datatemp});
    });
});
router.post('/find',function(req,res,next)
{
    tblModel.findById(req.body.id, function (err, datatemp)
    {
        res.send({validate:true,data:datatemp});
    });
})
router.post('/save', function(req, res, next)
{
    let rq = req.body;
    let datasave =
    {
        _id: new mongoose.Types.ObjectId(),
        nombre_primero      :rq.nombre_primero,
        nombre_segundo      :rq.nombre_segundo,
        apellido_primero    :rq.apellido_primero,
        apellido_segundo    :rq.apellido_segundo,
        documento_numero    :rq.documento_numero
    };
    tblModel.create(datasave,function(error,response)
    {
        res.send({validate:true,data:response});
    })
});
module.exports = router;