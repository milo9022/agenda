var mongoose     = require('mongoose');
const db         = require('../config/index');
var express      = require('express');
var router       = express.Router();
let documentType = require('../models/coll_documents_types');
let citas        = require('../models/coll_citas');
let tblPatient   = require('../models/coll_patients');
let updates  = require('../controls/updates');

router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
  });




function updateothers(data,_id)
{
    console.log({data:data,_id:_id});
    citas.
    updateMany({_id:new mongoose.Types.ObjectId(_id)},data)
}
router.post('/', function(req, res, next) 
{
  tblPatient.paginate(
    {}, 
    { page: req.body.pag, limit: 10, sort:{nombre_primero:1,nombre_segundo:1,apellido_primero:1,apellido_segundo:1}} 
  ,function (err, datatemp)
  {
      if (err) return handleError(err);
      res.send({validate:true,data:datatemp});
  });
});
router.post('/find',function(req,res,next)
{
    tblPatient.findById(req.body.id, function (err, datatemp)
    {
        res.send({validate:true,data:datatemp});
    });
})
router.post('/documento',function(req,res,next)
{
    tblPatient.findOne({documento:req.body.documento}, function (err, datatemp)
    {
        res.send({validate:true,data:datatemp});
    });
});
router.post('/save', async function(req, res, next)
{
  
  let rq = req.body;
    tblPatient.findOne({documento:rq.documento}, async function (err, datatemp)
    {
        if(datatemp==null)
        {
          var documentTypes = await new Promise((resolve, reject) => 
          {
            if(rq.documento_typo_id==null)
            {
              resolve({_id : null,name : "--",name_short : "--"});
            }
            else
            {
              documentType.findById(rq.documento_typo_id, function(err, resultado) 
              {
                if(typeof resultado!='undefined')
                {
                  if (err){ reject(err)}else{resolve(resultado)}
                }
                else
                {
                  resolve({data:null});
                }
              });
            }
          });
          let datasave =
          {
              _id:                new mongoose.Types.ObjectId(),
              documento:          rq.documento,
              nombre_primero:     rq.nombre_primero,
              nombre_segundo:     rq.nombre_segundo,
              apellido_primero:   rq.apellido_primero,
              apellido_segundo:   rq.apellido_segundo,
              fecha_nacimiento:   rq.fecha_nacimiento,
              email:              rq.email,
              numero_contacto:    rq.numero_contacto,
              pais:               rq.pais,
              ciudad:             rq.ciudad,
              documentoType:      documentTypes
          };
          tblPatient.create(datasave,function(error,value)
          {
              res.send({new:true});
          })
        }
        else{
          res.send({new:false})
        }
    });
});
router.post('/saveMany', async function(req, res, next)
{
  
  let rqs = req.body;
  for(var i = 0;i<rqs.length;i++)
  {
    let rq=rqs[i];
    documentTypes = await new Promise((resolve, reject) => 
    {
      if(rq.documento_typo_id==null)
      {
        resolve({_id : null,name : "--",name_short : "--"});
      }
      else
      {
        documentType.findById(rq.documento_typo_id, function(err, resultado) 
        {
          if(typeof resultado!='undefined')
          {
            if (err){ reject(err)}else{resolve(resultado)}
          }
          else
          {
            resolve({data:null});
          }
        });
      }
    });
    rq.documentoType=documentTypes;
    await tblPatient.updateOne(
      {documento:rq.documento},
      rq, 
      {
        // Return the document after updates are applied
        new: true,
        // Create a document if one isn't found. Required
        // for `setDefaultsOnInsert`
        upsert: true,
        setDefaultsOnInsert: true
      });
  }
  res.send({validate:true});
});

router.post('/update', async function(req, res, next)
{
    let rq = req.body;
    var documentTypes = await new Promise((resolve, reject) => 
    {
        documentType.findById(rq.documento_typo_id, function(err, resultado) 
      {
        if(typeof resultado!='undefined')
        {
          if (err){ reject(err)}else{resolve(resultado)}
        }
        else
        {
          resolve({data:null});
        }
      });
    });
    let datasave =
    {
        _id:                new mongoose.Types.ObjectId(rq._id),
        documento:          rq.documento,
        nombre_primero:     rq.nombre_primero,
        nombre_segundo:     rq.nombre_segundo,
        apellido_primero:   rq.apellido_primero,
        apellido_segundo:   rq.apellido_segundo,
        fecha_nacimiento:   rq.fecha_nacimiento,
        email:              rq.email,
        numero_contacto:    rq.numero_contacto,
        pais:               rq.pais,
        ciudad:             rq.ciudad,
        documentoType:      documentTypes
    };
    let paciente = await updates.updatePaciente(rq._id,datasave);
    res.send({validate:true,paciente:paciente});
});
router.post('/delete', function(req, res, next)
{
    let rq = req.body;
    tblPatient.remove({_id:new mongoose.Types.ObjectId(rq._id)},function(error)
    {
        res.send({error:error});
    }).catch(function(datares)
    {
        res.send({catch:datares});
    });
});
module.exports = router;