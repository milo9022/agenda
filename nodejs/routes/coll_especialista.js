
var mongoose         = require('mongoose'); 
var express          = require('express');
var router           = express.Router();
let tblModel         = require('../models/coll_especialista');
let tblDocumentTypes = require('../models/coll_documents_types');
let tblSpecialties   = require('../models/coll_specialties');
let updates          = require('../controls/updates');

router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
  });
/* GET users listing. */
router.get('/', function(req, res, next) 
{
    tblModel.find({}, function (err, datatemp)
    {
        if (err) return handleError(err);
        res.send({validate:true,data:datatemp});
    });
});
router.post('/find',function(req,res,next)
{
    tblModel.findById(req.body.id, function (err, datatemp)
    {
        res.send({validate:(typeof datatemp !== "undefined"),data:datatemp});
    });
})
router.post('/save', function(req, res, next)
{
    let rq = req.body;
    tblDocumentTypes.findById(rq.documento_typo_id)
    .then((documento_typo)=>
    {
        tblSpecialties.findById(rq.especialidad_id)
        .then((especialidad)=>
        {   
            let datasave =
            {
                _id: new mongoose.Types.ObjectId(),
                nombre_primero      :rq.nombre_primero,
                nombre_segundo      :rq.nombre_segundo,
                apellido_primero    :rq.apellido_primero,
                apellido_segundo    :rq.apellido_segundo,
                documento_numero    :rq.documento_numero,
                especialidad_tipo   :especialidad,
                documento_tipo      :documento_typo
            };
            tblModel.create(datasave,function(error,response)
            {
                res.send({validate:true});
            });
        });
    });
});
router.post('/update', function(req, res, next)
{
    let rq = req.body;
    tblDocumentTypes.findById(rq.documento_typo_id)
    .then((documento_typo)=>
    {
        tblSpecialties.findById(rq.especialidad_id)
        .then((especialidad)=>
        {   
            let datasave =
            {
                'nombre_primero'      :rq.nombre_primero,
                'nombre_segundo'      :rq.nombre_segundo,
                'apellido_primero'    :rq.apellido_primero,
                'apellido_segundo'    :rq.apellido_segundo,
                'documento_numero'    :rq.documento_numero,
                'especialidad_tipo'   :especialidad,
                'documento_tipo'      :documento_typo
            }
            tblModel
            .updateOne({_id:rq._id},datasave,function(error,response)
            {
                res.send({datasave,response});
            })
            .catch(function(datares)
            {
                console.log({error:datares});
            });
        }).catch(function(datares)
        {
            console.log(datares);
        });;
    }).catch(function(datares)
    {
        console.log(datares);
    });
});

router.post('/delete', function(req, res, next)
{
    let rq = req.body;
    tblModel.remove({_id:new mongoose.Types.ObjectId(rq._id)},function(error)
    {
        res.send({error:error});
    }).catch(function(datares)
    {
        res.send({catch:datares});
    });
});

module.exports = router;