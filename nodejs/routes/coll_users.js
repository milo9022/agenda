
var mongoose = require('mongoose'); 
var express  = require('express');
var router   = express.Router();
let documentType    = require('../models/coll_documents_types');
let tblModel = require('../models/coll_users');
let citas           = require('../models/coll_citas');
var bcrypt = require('bcrypt');
router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
  });

//FUNCTIONS
/* GET users listing. */
router.get('/', function(req, res, next) 
{
    tblModel.find({}, function (err, datatemp)
    {
        if (err) return handleError(err);
        res.send({validate:true,data:datatemp});
    });
});
router.post('/find',function(req,res,next)
{
    tblModel.findById(req.body.id, function (err, datatemp)
    {
        res.send({validate:true,data:datatemp});
    });
})
router.post('/save', async function(req, res, next)
{
    let rq = req.body;
    var documentTypes = await new Promise((resolve, reject) => 
    {
      documentType.findById(rq.documento_typo_id, function(err, resultado) 
      {
        if(typeof resultado!='undefined')
        {
          if (err){ reject(err)}else{resolve(resultado)}
        }
        else
        {
          resolve({data:null});
        }
      });
    });
    let password = bcrypt.hashSync(rq.documento_numero, 10);
    let datasave =
    {
        _id: new mongoose.Types.ObjectId(),
        nombre_primero:     rq.nombre_primero,
        nombre_segundo:     rq.nombre_segundo,
        apellido_segundo:   rq.apellido_segundo,
        apellido_primero:   rq.apellido_primero,
        documento_numero:   rq.documento_numero,
        email:              rq.email,
        documentoType:      documentTypes,
        password:           password
    };
    tblModel.create(datasave,function(error,response)
    {
        res.send({validate:true,data:response});
    })
});
router.post('/update', async function(req, res, next)
{
    let rq = req.body;
    var documentTypes = await new Promise((resolve, reject) => 
    {
        documentType.findById(rq.documento_typo_id, function(err, resultado) 
      {
        if(typeof resultado!='undefined')
        {
          if (err){ reject(err)}else{resolve(resultado)}
        }
        else
        {
          resolve({data:null});
        }
      });
    });
    let datasave =
    {
        nombre_primero:     rq.nombre_primero,
        nombre_segundo:     rq.nombre_segundo,
        apellido_segundo:   rq.apellido_segundo,
        apellido_primero:   rq.apellido_primero,
        documento_numero:   rq.documento_numero,
        email:              rq.email,
        documentoType:      documentTypes
    };
    tblModel
    .updateOne({_id:rq._id},datasave,function(error,response)
    {
        //updateothers(datasave,rq._id);
        res.send({datasave,response});
    })
    .catch(function(datares)
    {
        console.log({error:datares});
    });
});
router.post('/delete', function(req, res, next)
{
    let rq = req.body;
    tblModel.remove({_id:new mongoose.Types.ObjectId(rq._id)},function(error)
    {
        res.send({error:error});
    }).catch(function(datares)
    {
        res.send({catch:datares});
    });
});
module.exports = router;