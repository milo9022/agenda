const db            = require('../config/index');
var mongoose        = require('mongoose'); 
let tblPatient      = require('../models/coll_patients');
let tblEspecilaista = require('../models/coll_especialista');
let tblEspecialidad = require('../models/coll_specialties');

let documentType    = require('../models/coll_documents_types');
let tblCitas        = require('../models/coll_citas');
let tblEps          = require('../models/coll_eps')
let citasEstado     = require('../models/coll_citas_estado');

let functions = 
{
  async updateEps(_id,datasave)
  {
    var result = await new Promise((resolve, reject) => 
    {
      tblEps
      .updateOne(
          {_id:_id},
          datasave, 
          function(err, resultado) 
        {
            if(typeof resultado!='undefined')
            { 
              if (err){ reject(err)}else{resolve(resultado)}
            }
            else
            {
              resolve({data:null});
            }
        });
    });
    var citas =  await new Promise((resolve, reject) => 
    {
      db.collection('coll_citas')
      .updateMany
      (
        {"eps._id": new mongoose.Types.ObjectId(_id)},
        {$set:{eps:datasave}},
        function(err, resultado) 
        {
            if(typeof resultado!='undefined')
            {
              if (err){ reject(err)}else{resolve(resultado)}
            }
            else
            {
              resolve({data:null});
            }
        }
      );
    });
  },
  async updateEspecialidades(_id,datasave)
  {
    var result = await new Promise((resolve, reject) => 
    {
      db
      .collection('coll_specialties')
      .updateOne(
        {_id:new mongoose.Types.ObjectId(_id)},
        {$set:datasave},
        function(err, resultado) 
        {
            if(typeof resultado!='undefined')
            { 
              if (err){ reject(err)}else{resolve(resultado)}
            }
            else
            {
              resolve(null);
            }
        });
    });
    await new Promise((resolve, reject) => 
    {
      db.collection('coll_especialistas')
      .updateMany
      (
        {"especialidad_tipo._id": new mongoose.Types.ObjectId(_id)},
        {$set:{especialidad_tipo:datasave}},
        function(err, resultado) 
        {
            if(typeof resultado!='undefined')
            {
              if (err){ reject(err)}else{resolve(resultado)}
            }
            else
            {
              resolve({data:null});
            }
        }
      );
    });
    var coll_especialidad =  await new Promise((resolve, reject) => 
    {
      tblEspecialidad
      .findById(_id,
        function(err, resultado) 
        {
            if(typeof resultado!='undefined')
            {
              if (err){ reject(err)}else{resolve(resultado)}
            }
            else
            {
              resolve({data:null});
            }
        }
      );
    });
    
    var citas =  await new Promise((resolve, reject) => 
      {
        db
        .collection('coll_citas')
        .updateMany(
          {"especialista.especialidad_tipo._id": new mongoose.Types.ObjectId(_id)},
          {$set:{'especialista.especialidad_tipo':coll_especialidad}},
          function(err, resultado) 
          {
              if(typeof resultado!='undefined')
              {
                if (err){ reject(err)}else{resolve(resultado)}
              }
              else
              {
                resolve({data:null});
              }
          }
        );
      });
    
    return {data:'update'};
  },
  async updatePaciente(_id,datasave)
    {
      var result = await new Promise((resolve, reject) => 
      {
        tblPatient.updateOne({_id:_id},datasave, function(err, resultado) 
          {
              if(typeof resultado!='undefined')
              { 
                if (err){ reject(err)}else{resolve(resultado)}
              }
              else
              {
                resolve({data:null});
              }
          });
      });
      var citas =  await new Promise((resolve, reject) => 
      {
        db.collection('coll_citas')
        .updateMany
        (
          {"paciente._id": new mongoose.Types.ObjectId(_id)},
          {$set:{paciente:datasave}},
          function(err, resultado) 
          {
              if(typeof resultado!='undefined')
              {
                if (err){ reject(err)}else{resolve(resultado)}
              }
              else
              {
                resolve({data:null});
              }
          }
        );
      });
    }
    



};
module.exports = functions;
