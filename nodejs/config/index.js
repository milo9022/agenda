'use strict'
const host='127.0.0.1';
const port='';
const database='agenda';
const useNewUrlParser= true;
const useUnifiedTopology= true;
const debuger=false;

var mongoose = require('mongoose'); 
mongoose.connect('mongodb://'+host+'/'+database, {
  useNewUrlParser:  useNewUrlParser,
  useUnifiedTopology: useUnifiedTopology
});
mongoose.set('debug',debuger);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


module.exports = db;
