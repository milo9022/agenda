'use strict'

// Cargamos el módulo de mongoose
var mongoose = require('mongoose');
 
// Usaremos los esquemas
var Schema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name:String,
    name_short:String
});
// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('coll_documents_type', Schema);
