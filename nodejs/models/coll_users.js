'use strict'

// Cargamos el módulo de mongoose
var mongoose = require('mongoose');
// Usaremos los esquemas
var Schema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nombre_primero:String,
    nombre_segundo:String,
    apellido_segundo:String,
    apellido_primero:String,
    email:String,
    password:String,
    documentoType:Object,
    documento_numero:String,
});
Schema.methods.encryptPassword = (password)=>{
    return bcryp.hashSync(password,bcryp.genSaltdSync(10));
}
Schema.methods.comparePassword = function(password)
{
    return bcryp.compareSync(password, this.password);
};

// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('coll_user', Schema);
