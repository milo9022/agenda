'use strict'

// Cargamos el módulo de mongoose
var mongoose = require('mongoose');
 
// Usaremos los esquemas
var Schema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nombre_primero:String,
    nombre_segundo:String,
    apellido_primero:String,
    apellido_segundo:String,
    documento_numero:String,
    documento_tipo:Object,
    especialidad_tipo:Object
});
// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('coll_especialista', Schema);
