'use strict'

// Cargamos el módulo de mongoose
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

// Usaremos los esquemas
var Schema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    paciente:Object,
    especialista:Object,
    eps:Object,
    tipocita:String,
    fecha:Date,
    hora:String,
    observacion:String,
    estado:Object
});
// Exportamos el modelo para usarlo en otros ficheros
Schema.plugin(mongoosePaginate);
module.exports = mongoose.model('coll_cita', Schema);
