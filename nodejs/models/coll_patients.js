'use strict'

// Cargamos el módulo de mongoose
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

// Usaremos los esquemas
var Schema = mongoose.Schema({
    _id:                mongoose.Schema.Types.ObjectId,
    documentoType:      Object,// {"_id","name","name_short"},
    documento:          String,
    nombre_primero:     String,
    nombre_segundo:     String,
    apellido_primero:   String,
    apellido_segundo:   String,
    fecha_nacimiento:   Date,
    email:              String,
    numero_contacto:    String,
    pais:               String,
    ciudad:             String
});
// Exportamos el modelo para usarlo en otros ficheros
Schema.plugin(mongoosePaginate);
module.exports = mongoose.model('coll_patients', Schema);
