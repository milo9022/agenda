'use strict'

// Cargamos el módulo de mongoose
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
// Usaremos los esquemas
var Schema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    description:String,
    fecha:Date
});
// Exportamos el modelo para usarlo en otros ficheros
Schema.plugin(mongoosePaginate);
module.exports = mongoose.model('coll_sugerencia', Schema);